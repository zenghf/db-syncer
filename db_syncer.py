from steemdata import SteemData
from pymongo import MongoClient, ASCENDING, DESCENDING
import datetime
import sys

LIMIT_PER_FETCH = 100000
MONGO_URI = 'mongodb+srv://BiceSteemServer:BiceNo1@bice-steem-data-dump-htnnn.mongodb.net/'


class SteemDataFetcher():

  def __init__(self):
    self.steem_data = SteemData()

  def get_operations(self, offset=0):
    return self.steem_data.Operations.find({}, projection={'_id': 0}, limit=LIMIT_PER_FETCH, skip=offset)


class MongoDumper():

  def __init__(self, steem_data_bice_db, is_dry_run=True):
    self.steem_data_bice_db = steem_data_bice_db
    self.is_dry_run = is_dry_run

  def dump_operations(self, operations, offset=0):
    ops = []
    index = 1
    for op in operations:
      op['id'] = offset + index # Add an extra 'id' atribute to keep track of the order.
      index = index + 1
      ops.append(op)

    num_of_ops = len(ops)
    if num_of_ops == 0:
      print('Nothing to dump')
    else:
      if not self.is_dry_run:
        self.steem_data_bice_db.Operations.insert_many(ops)
      firstOp = ops[0]
      lastOp = ops[num_of_ops - 1]
      message = '{}: Inserted {:d} operations from block: {:d} to block: {:d}'
      print(message.format(datetime.datetime.now(), num_of_ops, firstOp['block_num'], lastOp['block_num']))
    return num_of_ops


class DBSyncer():

  def __init__(self, is_dry_run=True):
    self.steem_data_bice_db = MongoClient(MONGO_URI).steem_data_bice_db
    print('Connected to Bice steem dump data mongodb. Is dry run: {}'.format(is_dry_run))
    self.mongo_dumper = MongoDumper(self.steem_data_bice_db, is_dry_run)
    self.steem_data_fetcher = SteemDataFetcher()

  def setup_database(self):
    collectionNames = self.steem_data_bice_db.collection_names()

    if "Operations" not in collectionNames:
      self.steem_data_bice_db.create_collection("Operations")
      self.steem_data_bice_db.Operations.create_index([("id", ASCENDING)], name="ID", background=True, unique=True)
      self.steem_data_bice_db.Operations.create_index(
        [("type", ASCENDING), ("id", ASCENDING)], name="Type_ID", background=True, unique=True)
      self.steem_data_bice_db.Operations.create_index(
        [("timestamp", ASCENDING), ("id", ASCENDING)], name="Timestamp_ID", background=True, unique=True)
      self.steem_data_bice_db.Operations.create_index(
        [("block_num", ASCENDING), ("id", ASCENDING)], name="BlockNum_ID", background=True, unique=True)

  # def optional_setup(self):
  #   # self.steem_data_bice_db.Operations.drop_index("type_1_timestamp_1_block_num_1")
  #   # self.steem_data_bice_db.Operations.drop()
  #   sys.exit(0)

  def sync_operations(self):
    # Checkpointing. Do not start from the very beginning but start from the last checkpoint instead.
    offset = self.__get_current_max_operation_id_in_bice_dump()
    print("Current max operation id: {:d}, continue from this point.".format(offset))

    while True:
      ops = self.steem_data_fetcher.get_operations(offset)
      num_of_dumped_rows = self.mongo_dumper.dump_operations(ops, offset)
      if num_of_dumped_rows < LIMIT_PER_FETCH:
        print('Dumping operations finished')
        break
      else:
        offset += LIMIT_PER_FETCH

  def __get_current_max_operation_id_in_bice_dump(self):
    op = self.steem_data_bice_db.Operations.find({}, projection={'id': 1}).sort("id", DESCENDING).limit(1)
    for only_op in op:
      return only_op['id']

    return 0



if __name__ == "__main__":
  dbSyncer = DBSyncer(is_dry_run=False) # Will not dump data to mongodb if in dryrun mode.
  dbSyncer.setup_database()
  # dbSyncer.optional_setup()
  dbSyncer.sync_operations()
