from steemdata import SteemData
from pymongo import MongoClient, ASCENDING, DESCENDING
import datetime
import sys
from pistonapi.steemnoderpc import SteemNodeRPC
import json

LIMIT_PER_FETCH = 100
MONGO_URI = 'mongodb+srv://BiceSteemServer:BiceNo1@bice-steem-data-dump-htnnn.mongodb.net/'

class SteemDataFetcher():

  def __init__(self):
    self.steem_data = SteemData()

  def get_operations(self, offset=0):
    return self.steem_data.Operations.find({}, projection={'_id': 0}, limit=LIMIT_PER_FETCH, skip=offset)

class BlockFetcher():

  def __init__(self):
    self.steem_node_rpc = SteemNodeRPC("wss://steemd.privex.io")

  def get_block(self, start, end):
    blocks = self.steem_node_rpc.block_stream(start=start, stop=end)
    block_array = []
    for block in blocks:
      block_array.append(block)
      # print(block)
    return block_array
    #  for transaction in block['transactions']:
    #   print(transaction)
    #   print("\n")

class MongoDumper():

  def __init__(self, steem_data_bice_db, is_dry_run=True):
    self.steem_data_bice_db = steem_data_bice_db
    self.is_dry_run = is_dry_run

  def dump_blocks(self, blocks, offset=0):
    num = 0
    if not self.is_dry_run:
      for block in blocks:

        try:
          result = self.steem_data_bice_db.Blocks.insert(block)
        except:
          print ("error result :", block['block_num'])
          print("Unexpected error:", sys.exc_info()[0])
        num = num + 1
        # print(block)

    message = '{}: Inserted {:d} , num of block {:d}'
    print(message.format(datetime.datetime.now(), offset, num))

    return num


class DBSyncer():

  def __init__(self, is_dry_run=True):
    self.steem_data_bice_db = MongoClient(MONGO_URI).steem_data_bice_db
    print('Connected to Bice steem dump data mongodb. Is dry run: {}'.format(is_dry_run))
    self.mongo_dumper = MongoDumper(self.steem_data_bice_db, is_dry_run)
    self.steem_data_fetcher = SteemDataFetcher()
    self.block_fetcher = BlockFetcher()

  def setup_database(self):
    collectionNames = self.steem_data_bice_db.collection_names()

    if "Blocks" not in collectionNames:
      self.steem_data_bice_db.create_collection("Blocks")
      self.steem_data_bice_db.Blocks.create_index(
        [("block_id", ASCENDING), ("id", ASCENDING)], name="Block_ID", background=True, unique=True)
      self.steem_data_bice_db.Blocks.create_index(
        [("timestamp", ASCENDING), ("id", ASCENDING)], name="Timestamp_ID", background=True, unique=True)
      self.steem_data_bice_db.Blocks.create_index(
        [("block_num", ASCENDING), ("id", ASCENDING)], name="BlockNum_ID", background=True, unique=True)


  def optional_setup(self):
    # self.steem_data_bice_db.Blocks.drop_index("ID")
    self.steem_data_bice_db.Blocks.drop()
    # self.steem_data_bice_db.Blocks.create_index(
    #     [("block_id", ASCENDING), ("id", ASCENDING)], name="Block_ID", background=True, unique=True)
    # self.steem_data_bice_db.Blocks.create_index(
    #   [("timestamp", ASCENDING), ("id", ASCENDING)], name="Timestamp_ID", background=True, unique=True)
    # self.steem_data_bice_db.Blocks.create_index(
    #   [("block_num", ASCENDING), ("id", ASCENDING)], name="BlockNum_ID", background=True, unique=True)

    sys.exit(0)

  def sync_blocks(self):
    offset = self.__get_current_max_block_id_in_bice_dump()
    print("Current max block id: {:d}, continue from this point.".format(offset))


    while True:
      blocks = self.block_fetcher.get_block(offset-LIMIT_PER_FETCH, offset)
      print("start: {:d}, end: {:d}".format(offset-LIMIT_PER_FETCH, offset))

      num_of_dumped_rows = self.mongo_dumper.dump_blocks(blocks, offset)
      print("Current fetch number. {:d}".format(num_of_dumped_rows))

      if num_of_dumped_rows < LIMIT_PER_FETCH:
        print('Dumping operations finished')
        break
      else:
        offset -= num_of_dumped_rows

  def __get_current_max_block_id_in_bice_dump(self):
    # block = self.steem_data_bice_db.Blocks.find({}, projection={'block_num': 1}).sort("block_num", ASCENDING).limit(1)
    # for only_block in block:
    #   return only_block['block_num']

    return 21713303


def connect(self):
    self.steem_node_rpc = SteemNodeRPC("wss://steemd.privex.io")

def get_block(self, start, end):
  blocks = self.steem_node_rpc.block_stream(start=start, stop=end)
  for block in blocks:
    print(block)


if __name__ == "__main__":
  dbSyncer = DBSyncer(is_dry_run=False) # Will not dump data to mongodb if in dryrun mode.
  # dbSyncer.optional_setup()

  dbSyncer.setup_database()
  dbSyncer.sync_blocks()
