
# coding: utf-8

# In[1]:

import numpy as np # 数组常用库 

import pandas as pd # 读入csv常用库

import matplotlib.pyplot as plt # 画图常用库


# In[3]:

data = pd.read_csv('./post_author_data_full.csv')


# In[6]:

df = data


# In[7]:

n_users = df.voter.unique().shape[0]
n_items = df.post_id.unique().shape[0]


# In[10]:

print ('Number of users = ' + str(n_users) + ' | Number of posts = ' + str(n_items))


# In[ ]:

from sklearn import cross_validation as cv
train_data, test_data = cv.train_test_split(df, test_size=0.25)


# In[ ]:

#Create two user-item matrices, one for training and another for testing
train_data_matrix = np.zeros((n_users, n_items))
for line in train_data.itertuples():
    train_data_matrix[line[1]-1, line[2]-1] = line[3]

test_data_matrix = np.zeros((n_users, n_items))
for line in test_data.itertuples():
    test_data_matrix[line[1]-1, line[2]-1] = line[3]


# In[ ]:

from sklearn.metrics.pairwise import pairwise_distances
user_similarity = pairwise_distances(train_data_matrix, metric='cosine')
item_similarity = pairwise_distances(train_data_matrix.T, metric='cosine')


# In[ ]:

def predict(ratings, similarity, type='user'):
    if type == 'user':
        mean_user_rating = ratings.mean(axis=1)
        #You use np.newaxis so that mean_user_rating has same format as ratings
        ratings_diff = (ratings - mean_user_rating[:, np.newaxis])
        pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array([np.abs(similarity).sum(axis=1)]).T
    elif type == 'item':
        pred = ratings.dot(similarity) / np.array([np.abs(similarity).sum(axis=1)])
    return pred


# In[ ]:

item_prediction = predict(train_data_matrix, item_similarity, type='item')
user_prediction = predict(train_data_matrix, user_similarity, type='user')


# In[ ]:

from sklearn.metrics import mean_squared_error
from math import sqrt
def rmse(prediction, ground_truth):
    prediction = prediction[ground_truth.nonzero()].flatten()
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))

