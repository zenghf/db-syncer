# This file is to dump the

from pymongo import MongoClient, ASCENDING, DESCENDING
import datetime
import sys
import json
import numpy as np
import pandas as pd
import pprint
import csv

LIMIT_PER_FETCH = 1
MONGO_URI = 'mongodb+srv://BiceSteemServer:BiceNo1@bice-steem-data-dump-htnnn.mongodb.net/'

class DBUserProfile():

  def __init__(self, is_dry_run=True):
    self.steem_data_bice_db = MongoClient(MONGO_URI).steem_data_bice_db
    print('Connected to Bice steem dump data mongodb. Is dry run: {}'.format(is_dry_run))
    # self.mongo_dumper = MongoDumper(self.steem_data_bice_db, is_dry_run)

  def setup_database(self):
    collectionNames = self.steem_data_bice_db.collection_names()


    if "Account_profiles" not in collectionNames:
      self.steem_data_bice_db.create_collection("Account_profiles")

      # self.steem_data_bice_db.Account_profiles.create_index(
      #   [("id", ASCENDING), ("id", ASCENDING)], name="id", background=True, unique=True)
      # self.steem_data_bice_db.Blocks.create_index(
      #   [("timestamp", ASCENDING), ("id", ASCENDING)], name="Timestamp_ID", background=True, unique=True)
      # self.steem_data_bice_db.Blocks.create_index(
      #   [("block_num", ASCENDING), ("id", ASCENDING)], name="BlockNum_ID", background=True, unique=True)

  def get_user_profile(self, is_test=False):
      Posts = self.steem_data_bice_db.Posts
      print("begin")

      active_votes = []
      # ans = None
      # if is_test:
      posts = Posts.find(
        {'id': { "$gt": 1 } },
        # {'id': 4 },

        {'active_votes':1, 'id':1},
      )


      with open( './data/post_author_data_full.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['post_id','voter','weight'])
        print('post_id,voter,weight,time')


        for p in posts:
          if p['id'] % 100 == 0:
            print(p['id'])
          for vote in p['active_votes']:
            message = '{},{},{},{}'

            # res = message.format(p['id'],vote['voter'],vote['weight'])
            # print(vote)
            writer.writerow([p['id'],vote['voter'],vote['weight']])



      # print(active_votes, len(active_votes))
      print("end")

if __name__ == "__main__":
  dbUserProfile = DBUserProfile(is_dry_run=False) # Will not dump data to mongodb if in dryrun mode.
  # dbSyncer.optional_setup()

  dbUserProfile.setup_database()
  dbUserProfile.get_user_profile()
